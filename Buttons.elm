import Html exposing (Html, beginnerProgram, div, button, text)
import Html.Events exposing (onClick)


model = 0


type Msg = Increment | Decrement


view model =
  div []
    [ button [ onClick Decrement ] [ text "-" ]
    , div [] [ text (toString model) ]
    , button [ onClick Increment ] [ text "+" ]
    ]

update msg model =
  case msg of
    Increment ->
      model + 1

    Decrement ->
      model - 1


main =
  beginnerProgram { model = model, view = view, update = update }
